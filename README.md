# Cheap Tiny Houses - Opportunity or Money Pit?

My wife and I had to go to our local grocery store on Saturday to pick up some basic items - bread, milk, tomatoes, etc. The store is closed on Sundays so Saturday afternoon they always reduce the price on selected perishable foods. It's very tempting to pick up some of these cheap items until one realizes that there's a good reason they are so heavily discounted.

That half-price loaf of bread has reached its "sell by" date. Before we can use the entire loaf it will probably become stale and we will have to discard least half of it. Likewise, that quart of milk is about to expire; before we get through half of it, the milk will most likely turn sour. And that shrink-wrapped package of a dozen tomatoes? They're already pretty soft - how will they be in a few days? Yuck!

Sometimes cheap is just cheap. The real estate market can be compared to the food market - there's always a reason that a discounted property is priced so low. Determining why [an inexpensive property is priced so low is critical in order to determine if it is truly "worth it"](http://www.tinyqualityhomes.org/118-incredible-diy-wood-pallet-furniture-ideas-and-projects/) to pursue. Obtaining the advice of a well qualified buyer's agent is very [wise move to make before committing to purchase a cheap tiny home](http://www.tinyqualityhomes.org/).

The reasons that most cheap properties are discounted can usually be classified in a few categories:

**1. The Handyman's Special**

Often times properties that have fallen into disrepair can be bought at prices significantly below the local market price of similar but well maintained properties. If the property owner is unable or unwilling to make the necessary repairs, their only recourse would be to offer it for sale at a much lower price.

If the prospect of investing "sweat equity" (i.e. manual labor) is not your [idea of tiny home ownership](http://www.tinyqualityhomes.org/ultimate-list-of-free-tiny-house-plans-updated-june-2017/), you may want to avoid this [type of inexpensive tiny home](https://www.pinterest.com/TinyQualityHomes/tiny-houses/). Likewise, if hiring contractors to perform the necessary repairs is not a viable option - just walk away. However, If the idea of doing the work yourself doesn't send shivers up your spine, a fixer upper or handyman special can be an excellent investment

**2. A Somewhat Questionable Neighborhood**

We've all heard the saying that the three most important aspects of real estate are location, location, location. Well, it's really true. The value of a property will vary quite a bit depending upon the location. This can be something to rejoice for the tiny homeowner in an upscale location. However, it can be financially devastating for a tiny homeowner in a neighborhood that is falling on hard times. Contrary to some people's belief, property values do not always increase with time.

In many urban locations, certain neighborhoods that have declined are gradually being revitalized through renovation of individual tiny homes. As these improvements spread the [value of tiny homes in the immediate area can begin to rise](http://www.nairaland.com/3857018/tiny-houses-tiny-house-movement). Your buyer's agent should be able to give you an idea about the direction that prices are moving so that you can make an informed decision about the true value of cheap tiny homes that are in this category.

**3. "Priced for Quick Sale"**

Circumstances may arise when a tiny homeowner must sell their property very quickly. These can include a need to liquidate assets for cash in hand, a requirement to relocate for employment purposes, or pressure to get out from under double mortgage payments after committing to the purchase of another tiny home.

Inexpensive tiny homes in this category usually provide the best value. However, these bargains do not normally remain on the market very long since a fast sale is the very reason that the property was discounted. The best approach to finding these fleeting opportunities as they arise is to have your buyer's agent notify you when new property listings hit the market. Most real estate agents have access to automation tools that will automatically notify you via email the same day that a property that meets your requirements is put up for sale. Without that type of competitive edge, it's likely that you'll never hear bout these prime opportunities.

**4. The Challenge of the Unknown**

This is the "catch-all" category for properties that don't fit in any of the other three categories. These tiny homes involve the most risk and should be considered with extreme caution. Remember, there is always a reason for a tiny house being under priced. If that reason is not apparent at [first glance you may have to do some serious research](http://www.musicblogging.org/) before even considering a purchase. Sellers are obligated by law to disclose any information that affects the tiny home's value. Make sure that you ask the right questions. Your buyer's agent will prove to be very helpful with this research.

Obtaining the assistance of a buyer's agent and investigating the reasons that a tiny home is priced lower than would normally be expected are the keys to determining the true value of a "cheap" tiny home. These deals can look very attractive at first but, with further investigation they can end up being either a "money pit" or a fabulous opportunity. It all depends on performing your due diligence.